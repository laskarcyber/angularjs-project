# Majalah Sunnah

Adalah aplikasi web yang memanfaatkan Situs Sunnah API sehingga tata letaknya menjadi sebuah majalah online. Aplikasi ini dibuat sebagai bahan untuk belajar AngularJS.

## Hal Yang Diperlukan

- [NodeJS](http://nodejs.org/) beserta NPM (Node Package Manager) nya. 
- [Yeoman](http://yeoman.io), [GruntJS](http://gruntjs.com) & [Bower](http://bower.io/)
- [Karma Runner](http://karma-runner.github.io/)

### Instalasi
Untuk instalasi NodeJS, silakan kunjungi halaman [download nya](http://nodejs.org/download/). 

Setelah `Node` & `npm` terinstall. Install paket-paket setelahnya secara global dengan 1 perintah:

    npm install -g yo grunt-cli bower generator-angular generator-karma

## Segera Memulai
Pastikan anda sudah menginstall hal-hal yang diperlukan, lakukan langkah-langkah berikut:

1. klon repositori ini
2. jalankan `npm install` dan `bower install`
3. Untuk menjalankan aplikasinya, jalankan `grunt server`.

## Lain-lain
Struktur awal basis kode ini dihasilkan dari perintah Yeoman `yo angular`.